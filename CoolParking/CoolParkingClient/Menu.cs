﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;

namespace CoolParkingClient
{
    public static class Menu
    {
        public static void RunMenu()
        {
            try
            {
                var logService = new LogService("Transaction.log");
                var parkingService = new ParkingService(new TimerService(), new TimerService(), logService);
                uint key;
                do
                {
                    Console.WriteLine("Welcome to the parking, to choose one of the oprtions, please press key from the menu below");
                    Console.WriteLine("Press 1 to check parking balance");
                    Console.WriteLine("Press 2 to see the amount of money parking has earned for last minute");
                    Console.WriteLine("Press 3 to check number of available lots at the parking");
                    Console.WriteLine("Press 4 to see all last minute transactions");
                    Console.WriteLine("Press 5 to see all transactions");
                    Console.WriteLine("Press 6 to add transport to the parking");
                    Console.WriteLine("Press 7 to remove transport from the parking");
                    Console.WriteLine("Press 8 to see balance of certain transport");
                    Console.WriteLine("Press 9 to replanish balance of certain transport");
                    Console.WriteLine("Press 0 to exit program");
                    Console.WriteLine("Enter key: ");
                    key = Convert.ToUInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            Console.WriteLine($"A parking has balance of {parkingService.GetBalance():C}");
                            break;
                        case 2:
                            Console.WriteLine($"A parking has earned {parkingService.GetAmountOfMoneyEarnedForLastPeriod():C}");
                            break;
                        case 3:
                            Console.WriteLine($"A parking has {parkingService.GetFreePlaces()} available lots" +
                                $"out of {parkingService.GetCapacity()} lots");
                            break;
                        case 4:
                            var transactions = parkingService.GetLastParkingTransactions();
                            Console.WriteLine("Transaction Time------------TransportId----------------Transaction Total");
                            foreach(TransactionInfo tr in transactions)
                            {
                                Console.WriteLine($"{tr.TransactionTime}----------{tr.TransportID}-----------{tr.Sum:C}");
                            }
                            break;
                        case 5:
                            Console.WriteLine($"All transactions: ");
                            var log = parkingService.ReadFromLog();
                            Console.Write(log);
                            break;

                        case 6:
                            Console.WriteLine("Please, enter your transport type. Available options are: car, truck, motorcycle, bus");
                            string type = Console.ReadLine();
                            Console.WriteLine("Please, enter your transport registration plate number");
                            string plateNumber = Console.ReadLine();
                            Console.WriteLine("Please, enter you transport balance");
                            decimal balance = Convert.ToDecimal(Console.ReadLine());
                            VehicleType vehicleType;
                            switch (type)
                            {
                                case "car":
                                    vehicleType = VehicleType.PassengerCar;
                                    break;
                                case "truck":
                                    vehicleType = VehicleType.Truck;
                                    break;
                                case "bus":
                                    vehicleType = VehicleType.Bus;
                                    break;
                                case "motorcycle":
                                    vehicleType = VehicleType.Motorcycle;
                                    break;
                                default:
                                    throw new ArgumentException("Invalid transport type");
                            }
                            parkingService.AddVehicle(new Vehicle(plateNumber, vehicleType, balance));
                            break;
                        case 7:
                            Console.WriteLine("Please, enter transport ID of a transport your want to remove.");
                            string idToRemove = Console.ReadLine();
                            parkingService.RemoveVehicle(idToRemove);
                            break;
                        case 8:
                            Console.WriteLine("Please, enter transport ID of a transport of which you want to check balance.");
                            string idToCheckBalance = Console.ReadLine();
                            Console.WriteLine("This transport has balance: " + parkingService.GetVehicleBalance(idToCheckBalance));
                            break;
                        case 9:
                            Console.WriteLine("Please, enter transport ID of a transport of which you want to replanish balance.");
                            string idToReplanishBalance = Console.ReadLine();
                            Console.WriteLine("Please, enter amount of your replanishment");
                            decimal replanishment = Convert.ToDecimal(Console.ReadLine());
                            parkingService.TopUpVehicle(idToReplanishBalance, replanishment);
                            break;
                        case 0:
                            break;
                        default:
                            Console.WriteLine("You entered invalid option, please try again, to exit press 0 after menu.");
                            break;
                    }


                } while (key != 0);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ ex.GetType()} says { ex.Message}");
                Console.ReadLine();
                RunMenu();
            }
        }
    }
}
