﻿using System;
namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime TransactionTime { get; private set; }
        public decimal Sum { get; private set; }
        public string TransportID { get; private set; }

        public TransactionInfo(decimal transferredMoney, string transportId)
        {
            TransactionTime = DateTime.Now;
            Sum = transferredMoney;
            TransportID = transportId;
        }
    }
}