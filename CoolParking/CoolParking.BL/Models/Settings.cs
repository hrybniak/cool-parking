﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal initialBalance = 0.0M;
        public static uint maxCapacity = 10;
        public static double billingPeriodInSeconds = 5.0;
        public static double loggingPeriodInSeconds = 60.0;
        public static decimal carParkingCost = 2.0M;
        public static decimal trackParkingCost = 5.0M;
        public static decimal busParkingCost = 3.5M;
        public static decimal motorcycleParkingCost = 1.0M;
        public static double penaltyRatio = 2.5;
    }
}