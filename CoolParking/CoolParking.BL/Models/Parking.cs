﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Lazy<Parking> lazy;
        private Parking() 
        {
            Vehicles = new List<Vehicle>();
            Transactions = new List<TransactionInfo>();
            Balance = Settings.initialBalance;
        }
        public static Parking Instanse 
        { 
            get 
            {
                if (lazy == null) lazy = new Lazy<Parking>(() => new Parking());
                return lazy.Value;
            } 
        }

        public decimal Balance { get; internal set; }

        internal List<Vehicle> Vehicles { get; }
        internal List<TransactionInfo> Transactions { get; }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    lazy = null;
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}