﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string path)
        {
            LogPath = path;
        }

        public string Read()
        {
            try
            {
                string log = "";
                using (FileStream fs = new FileStream(LogPath, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        log = sr.ReadToEnd();
                    }
                    return log;
                }
            }
            catch(FileNotFoundException ex)
            {
                throw new InvalidOperationException(ex.Message);
            }
        }

        public void Write(string logInfo)
        {
            using (FileStream fs = new FileStream(LogPath, FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        sw.WriteLine(logInfo);
                    }
                }
            
        }
    }
}